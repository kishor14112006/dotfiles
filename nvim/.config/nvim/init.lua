-- Use my .vimrc
vim.cmd('source ~/.vimrc')

-- Keybind Helpers
-- Compliments of: https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/
local map = function(key)
  -- get the extra options
  local opts = {noremap = true}
  for i, v in pairs(key) do
    if type(i) == 'string' then opts[i] = v end
  end

  -- basic support for buffer-scoped keybindings
  local buffer = opts.buffer
  opts.buffer = nil

  if buffer then
    vim.api.nvim_buf_set_keymap(0, key[1], key[2], key[3], opts)
  else
    vim.api.nvim_set_keymap(key[1], key[2], key[3], opts)
  end
end

-- Alias Plug command to make things look nicer.
local Plug = vim.fn['plug#']

-- Install plugins
vim.call('plug#begin', '~/.config/nvim/plugged')

-- Let's try TreeSitter...
Plug('nvim-treesitter/nvim-treesitter', {['do'] = ':TSUpdate'})

-- -----------------------------------------------------------------------------
-- ✂️ plugins-snippets
-- -----------------------------------------------------------------------------
Plug 'scrooloose/nerdcommenter'

-- -----------------------------------------------------------------------------
-- 🔤 plugins-autocomplete
-- -----------------------------------------------------------------------------
-- LSP Stuff
Plug 'neovim/nvim-lspconfig'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'folke/lsp-colors.nvim'
Plug 'folke/trouble.nvim'

-- Auto-Complete
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-emoji'
Plug 'f3Fora/cmp-spell'
Plug 'hrsh7th/nvim-cmp'

-- -----------------------------------------------------------------------------
-- ⚛️  plugins-syntax
-- -----------------------------------------------------------------------------
Plug 'posva/vim-vue'
Plug 'plasticboy/vim-markdown'
Plug 'masukomi/vim-markdown-folding'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'pangloss/vim-javascript'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'jparise/vim-graphql'
Plug 'cespare/vim-toml'
Plug 'habamax/vim-godot'

-- -----------------------------------------------------------------------------
-- 🛠️ plugins-tool
-- -----------------------------------------------------------------------------
-- File explorer
Plug 'francoiscabrol/ranger.vim'
Plug 'vifm/vifm.vim'
Plug 'rbgrouleff/bclose.vim'
-- Display Git status for each line in a buffer
Plug 'mhinz/vim-signify'
-- Git tools for Vim
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'shumphrey/fugitive-gitlab.vim'
-- Surround sections of text with things like brackets, quotes, etc.
Plug 'tpope/vim-surround'
-- Enhancements to Vim's sessions
Plug 'thaerkh/vim-workspace'
-- Indent guides
Plug 'nathanaelkane/vim-indent-guides'
-- Note-taking
Plug 'fmoralesc/vim-pad'
Plug 'vimwiki/vimwiki'
-- File / Buffer / Fuzzy Search
Plug ('junegunn/fzf', { ['dir'] = '~/.fzf', ['do'] = './install --all' })
Plug 'junegunn/fzf.vim'
-- Temporary buffer for note-taking
Plug 'mtth/scratch.vim'
-- Unix Utilties / Helpers
Plug 'tpope/vim-eunuch'
-- Better repeat support for compound commands
Plug 'tpope/vim-repeat'
-- Making moving around buffers easier.
Plug 'easymotion/vim-easymotion'
-- Plug 'justinmk/vim-sneak'
-- Search/Replace & Casing Tool
Plug 'tpope/vim-abolish'
Plug 'wakatime/vim-wakatime'
if (vim.fn.has('nvim') == 1) then
  Plug 'rhysd/vim-healthcheck'
end
Plug 'wincent/ferret'
-- Pair braces, quotes, etc.
Plug 'tmsvg/pear-tree'
-- Auto-format code.
Plug 'mhartington/formatter.nvim'
-- Themes/Colors/Etc.
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'srcery-colors/srcery-vim'
Plug 'morhetz/gruvbox'
Plug 'chrisbra/Colorizer'
Plug 'kshenoy/vim-signature'
Plug 'ryanoasis/vim-devicons'
Plug 'sainnhe/edge'
Plug 'jacoborus/tender.vim'
Plug 'Rigellute/shades-of-purple.vim'
Plug 'sainnhe/sonokai'

vim.call('plug#end')

-- -----------------------------------------------------------------------------
-- options-treesitter
-- -----------------------------------------------------------------------------
require('settings-treesitter')

-- -----------------------------------------------------------------------------
-- options-formatter
-- -----------------------------------------------------------------------------
require('settings-formatters')

-- -----------------------------------------------------------------------------
-- options-ferret
-- -----------------------------------------------------------------------------
-- Disable default keybinds.
vim.g.FerretMap = false

-- Search within the project/current working directory.
map({ 'n', '<space>sp', ':Ack<space>' })
-- Search for selected text
map({ 'v', '<space>sp', '"hy:Ack <c-r>h<CR>' })

vim.cmd([[
function SetFerretKeybinds ()
  let l:title = getqflist({'title': 1})['title']

  " Only set keybind if this is a search results window.
  if (l:title[0:5] == 'Search')
    nmap <space>sr <Plug>(FerretAcks)
  endif
endfunction

autocmd FileType qf autocmd BufEnter,BufRead <buffer> call SetFerretKeybinds()
]])

-- -----------------------------------------------------------------------------
-- 💄 plugins-cosmetic
-- -----------------------------------------------------------------------------
-- Color scheme options
vim.g.edge_style = 'neon'
vim.g.edge_disable_italic_comment = true

-- If you have vim >=8.0 or Neovim >= 0.1.5
if (vim.fn.has("termguicolors") == 1) then
  vim.cmd('set termguicolors')
end

-- Theme
vim.cmd('colorscheme sonokai')

-- -----------------------------------------------------------------------------
-- 🔤  options-pear-tree
-- -----------------------------------------------------------------------------
-- Always display pairs
vim.g.pear_tree_repeatable_expand = false

-- -----------------------------------------------------------------------------
-- 🔤 Auto-Complete
-- -----------------------------------------------------------------------------
vim.g.completeopt = { 'menu', 'menuone', 'noselect'}
require('settings-completion')

-- -----------------------------------------------------------------------------
-- 🔤  options-lsp
-- -----------------------------------------------------------------------------
require('settings-lsp')

-- -----------------------------------------------------------------------------
-- 🖋 options-vim-signature
-- -----------------------------------------------------------------------------
map({'n', '<space>ml', ':SignatureListBufferMarks<CR>'})

-- -----------------------------------------------------------------------------
-- 📄 options-vim-markdown
-- -----------------------------------------------------------------------------
-- The way this plugin interacts with the `foldlevel` setting does some weird
-- things (like collapse headings that the cursor isn't under).  In theory this
-- would be nice but it's really annoying to me so I'm just disabling the whole
-- thing for now.
vim.g.vim_markdown_folding_disabled = true

vim.g.vim_markdown_conceal = true
vim.g.vim_markdown_new_list_item_indent = false
vim.g.conceallevel = 2

-- -----------------------------------------------------------------------------
-- 👀 options-fzf
-- -----------------------------------------------------------------------------
vim.cmd([[
command! -bang CWDFiles call fzf#vim#files(
      \ expand('%:p:h'), 
      \ fzf#vim#with_preview(), 
      \ <bang>0)

function! GitCheckoutTo(refName)
  let refName = trim(a:refName)
  echomsg 'Checking out ref: ' . refName

  if refName =~ '^.*\/'
    echomsg 'Remote Detected!'
    let branchName = trim(system(
        \ 'git branch -a --list --format="%(refname:lstrip=-1)" ' . refName
        \))
    execute '!git checkout -b ' . branchName . ' --track ' . refName
  else
    echomsg 'Local Detected!'
    execute '!git checkout ' . refName
  endif
endfunction

function! GitCheckout()
  let branchNameCmd = 'git branch -a --format="%(refname:short)"'
  call fzf#run(
        \ fzf#wrap({
        \ 'options': ['--cycle', '--preview', 'git log --graph --color {}'],
        \ 'source': branchNameCmd, 
        \ 'sink': function('GitCheckoutTo')}))
endfunction

function! GetStashApplyIndex(arg)
  let stashIndex = system('echo ' . a:arg . ' | cut -d : -f 1')
  execute '!git stash apply ' . stashIndex
endfunction

function! GitStashApply()
  call fzf#run(fzf#wrap({
        \ 'options': [
          \ '--cycle', 
          \ '--preview', 
          \ 'grep -o "stash@{[0-9]\+}" <<< {} | xargs git stash show --format=format: -p --color=always'
          \ ], 
        \ 'source': 'git stash list', 
        \ 'sink': function('GetStashApplyIndex')}))
endfunction

" Hide the FZF status line.
autocmd! FileType fzf
autocmd  FileType fzf set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
]])

map({ 'n', '<space>ff', ':CWDFiles<CR>', noremap = true })
map({ 'n', '<space><space>', ':GFiles<CR>', noremap = true })
map({ 'n', '<space>fa', ':Files<CR>', noremap = true })
map({ 'n', '<space>fh', ':History<CR>', noremap = true })
map({ 'n', '<space>fb', ':Buffers<CR>', noremap = true })
map({ 'n', '<space>fl', ':Lines<CR>', noremap = true })
map({ 'n', '<space>fw', ':Windows<CR>', noremap = true })

-- [Buffers] Jump to the existing window if possible
vim.g.fzf_buffers_jump = true

-- Customize fzf colors to match your color scheme
vim.g.fzf_colors = {
  ['fg'] =      { 'fg', 'Normal' },
  ['bg'] =      { 'bg', 'Normal' },
  ['hl'] =      { 'fg', 'Comment' },
  ['fg+'] =     { 'fg', 'CursorLine', 'CursorColumn', 'Normal' },
  ['bg+'] =     { 'bg', 'CursorLine', 'CursorColumn' },
  ['hl+'] =     { 'fg', 'Statement' },
  ['info'] =    { 'fg', 'PreProc' },
  ['border'] =  { 'fg', 'Ignore' },
  ['prompt'] =  { 'fg', 'Conditional' },
  ['pointer'] = { 'fg', 'Exception' },
  ['marker'] =  { 'fg', 'Keyword' },
  ['spinner'] = { 'fg', 'Label' },
  ['header'] =  { 'fg', 'Comment' }
}

-- Hide preview by default.
vim.g.fzf_preview_window = { 'right:50%', 'ctrl-/' }

-- -----------------------------------------------------------------------------
-- 📔 options-vimwiki
-- -----------------------------------------------------------------------------
vim.cmd([[
let g:vimwiki_list = [
      \ {'path': '~/Seafile/default/Documents/vimwiki/',
      \ 'syntax': 'markdown', 'ext': '.md'},
      \ {'path': '~/Seafile/default/Documents/rpg-wiki/',
      \ 'syntax': 'markdown', 'ext': '.md'}
      \]
let g:vimwiki_folding = 'custom'
let g:vimwiki_global_ext = 1

" Disable some vimwiki keybinds that clash with my other keybinds.
let g:vimwiki_key_mappings = { 'lists': 0, }

" General/Global keybinds
nmap <leader>wl :VimwikiUISelect<CR>

function SetVimWikiKeybinds ()
  nmap <buffer> <leader>wt :VimwikiToggleListItem<CR>
  setlocal syntax=markdown
  setlocal foldmethod=expr

  " Prevent vimwiki from stealing <CR> when auto-complete pop-up is open.
  inoremap <silent><expr><buffer> <cr> pumvisible() ? coc#_select_confirm()
        \: "<C-]><ESC>:VimwikiReturn 1 1<CR>"
endfunction
autocmd FileType vimwiki call SetVimWikiKeybinds()

" For vim-markdown-folding
" autocmd Syntax markdown set foldexpr=StackedMarkdownFolds()
autocmd Syntax markdown set foldexpr=NestedMarkdownFolds()

" For plasticboy/vim-markdown
" autocmd Syntax markdown set foldexpr=Foldexpr_markdown(v:lnum)
]])

-- -----------------------------------------------------------------------------
-- 📔 options-vim-pad
-- -----------------------------------------------------------------------------
vim.g['pad#dir'] = '~/Seafile/default/Documents/vim-pad'
vim.g['pad#window_height'] = 20
vim.g['pad#title_first_line'] = 1
vim.g['pad#default_file_extension'] = '.md'
vim.g['pad#default_format'] = 'vimwiki'

-- Use RipGrep for searching
if (vim.fn.executable('rg') == 1) then
  vim.g['pad#search_backend'] = 'rg'
else
  print('vim-pad: RipGrep not found.  Attempting to fall back to grep...')
  if (vim.fn.has('macunix') == 1) then
    print('The OS X version of grep is incompatible with the GNU one vim-pad expects.  Please install rip-grep.')
  else
    vim.g['pad#search_backend'] = 'grep'
  end
end

-- Keybindings
vim.g['pad#set_mappings'] = 0
map({'n', '<space>nn', ':Pad new<CR>', noremap = true})
map({'n', '<space>nl', ':Pad ls<CR>', noremap = true})

-- -----------------------------------------------------------------------------
-- 📔 options-scratch-pad
-- -----------------------------------------------------------------------------
-- Disable default mappings
vim.g.scratch_no_mappings = false

-- Open Scratch Pad
map({'n', '<space>ns', ':Scratch<CR>', noremap = true})

-- -----------------------------------------------------------------------------
-- options-vim-indent-guides
-- -----------------------------------------------------------------------------
vim.g.indent_guides_enable_on_vim_startup = true
vim.g.indent_guides_start_level = 2
vim.g.indent_guides_guide_size = 2

-- Disable for terminal buffers.
vim.cmd([[
au TermEnter * IndentGuidesDisable
au TermLeave * IndentGuidesEnable
]])

-- -----------------------------------------------------------------------------
-- options-airline
-- -----------------------------------------------------------------------------
-- Turn off 'encoding' section.
vim.g.airline_section_x = vim.call('airline#section#create', {'Win:', '%{winnr()}'})
vim.g.airline_section_y = ''
vim.g.airline_section_z = vim.call('airline#section#create', {'linenr', 'maxlinenr', '%c'})
vim.g['airline#extensions#tabline#enabled'] = true

-- Set airline theme
vim.g.airline_theme = 'sonokai'

vim.g.airline_detect_modified = true
vim.g.airline_left_sep = ''
vim.g.airline_right_sep = ''
vim.g['airline#extensions#tabline#left_sep'] = ""
vim.g['airline#extensions#tabline#right_sep'] = ""
-- vim.g['airline#extensions#tabline#right_alt_sep'] = "\ue0ba"
vim.g['airline#extensions#tabline#left_alt_sep'] = ''

-- -----------------------------------------------------------------------------
-- options-vim-jsx
-- -----------------------------------------------------------------------------
vim.g.jsx_ext_required = false

-- -----------------------------------------------------------------------------
-- options-vim-workspace
-- -----------------------------------------------------------------------------
map({'n', '<leader>ws', ':ToggleWorkspace<CR>'})
vim.g.workspace_session_name = '.session.vim'
vim.g.workspace_session_disable_on_args = true
vim.g.workspace_autosave_ignore = {
  'gitcommit',
  'scratch',
  'pad',
  'qf',
  'help',
  'vim-plug'
}
vim.g.workspace_autosave_always = false
vim.g.workspace_autosave = false

-- -----------------------------------------------------------------------------
-- options-git-fugitive
-- -----------------------------------------------------------------------------
vim.cmd([[
autocmd User Fugitive command! -buffer -nargs=? Ggraphlog <mods> Git --paginate log --oneline --decorate --graph --all <args>
autocmd FileType fugitive nmap <buffer> za =
]])

map({'n', '<space>gg', ':Git<CR>', noremap = true})
map({'n', '<space>gb', ':Git blame<CR>', noremap = true})
map({'n', '<space>go', '<s-v>:GBrowse!<CR>', noremap = true})
map({'v', '<space>go', ':GBrowse!<CR>', noremap = true })
map({'n', '<space>gd', ':Gdiff<CR>', noremap = true })
map({'n', '<space>gl', ':Git log %<CR>', noremap = true })
map({'n', '<space>gL', ':Git log<CR>', noremap = true })
map({'n', '<space>gc', ':call GitCheckout()<CR>', noremap = true })
map({'n', '<space>gs', ':call GitStashApply()<CR>', noremap = true })
-- -----------------------------------------------------------------------------
-- options-vim-ranger
-- -----------------------------------------------------------------------------
vim.g.ranger_map_keys = false
map({'n', '<space>er', ':Ranger<CR>', noremap = true})

-- -----------------------------------------------------------------------------
-- options-vim-vifm
-- -----------------------------------------------------------------------------
map({'n', '<space>ee', ':Vifm<CR>', noremap = true})

-- -----------------------------------------------------------------------------
-- options-vim-easymotion
-- -----------------------------------------------------------------------------
-- Disable default mappings
vim.g.EasyMotion_do_mapping = false

-- Bi-directional, multi-line motion.
map({ 'n', 's', '<Plug>(easymotion-overwin-f)', noremap = false })
-- " Bi-direction and within-line 't' motion.
-- nmap t <Plug>(easymotion-bd-tl)

-- Use easymotion for search
map({ '',  '/', '<Plug>(easymotion-sn)', noremap = false })
map({ 'o', '/', '<Plug>(easymotion-tn)', noremap = false })

-- Search for selected text
map({ 'v', '<c-f>', '"hy/<c-r>h<CR>' })
-- Select word if no text is selected
map({ 'n', '<c-f>', 'viw"hy/<c-r>h<CR>' })

-- These `n` & `N` mappings are options. You do not have to map `n` & `N` to EasyMotion.
-- Without these mappings, `n` & `N` works fine. (These mappings just provide
-- different highlight method and have some other features )
map({ 'n', 'n', '<Plug>(easymotion-next)', noremap = false })
map({ 'n', 'N', '<Plug>(easymotion-prev)', noremap = false })

-- -----------------------------------------------------------------------------
-- options-nerd-commenter
-- -----------------------------------------------------------------------------
vim.cmd([[
function! NERDCommenter_before()
  if &ft == 'vue'
    let g:ft = 'vue'
    let stack = synstack(line('.'), col('.'))
    if len(stack) > 0
      let syn = synIDattr((stack)[0], 'name')
      if len(syn) > 0
        exe 'setf ' . substitute(tolower(syn), '^vue_', '', '')
      endif
    endif
  endif
endfunction
function! NERDCommenter_after()
  if g:ft == 'vue'
    setf vue
    let g:ft = ''
  endif
endfunction
]])

vim.g.NERDCreateDefaultMappings = false
vim.g.NERDSpaceDelims = true
vim.g.ft = ''

vim.g.NERDCustomDelimiters = {
  gdscript = {
    left = '# '
  }
}

map({'n', '<space>cc', ':call nerdcommenter#Comment("n", "toggle")<CR>' })
map({'v', '<space>cc', ':call nerdcommenter#Comment("x", "toggle")<CR> gv' })


-- -----------------------------------------------------------------------------
-- options-misc
-- -----------------------------------------------------------------------------
-- Use FileType Plug-ins
vim.cmd('filetype plugin on')

-- HACK: Uncomment this when running :PlugInstall or :PlugUpdate
-- Helps prevent an issue where running these commands hangs in WSL
-- vim.cmd([[
  -- let g:loaded_clipboard_provider = 1
  -- let $NVIM_NODE_LOG_FILE='nvim-node.log'
  -- let $NVIM_NODE_LOG_LEVEL='warn'
-- ]])

-- Explicitly set python paths for OSX
if(vim.fn.has('macunix') == 1) then
  vim.g.python_host_prog = '~/.pyenv/shims/python'
  vim.g.python3_host_prog = '~/.pyenv/shims/python3'
end

-- Show config directory
map({'n', '<leader>co', ':e ~/.dotfiles/dotfiles/home<CR>'})

-- Always show the signcolumn, otherwise it would shift the text each time
-- diagnostics appear/become resolved.
if (vim.fn.has("patch-8.1.1564") == true) then
  -- Recently vim can merge signcolumn and number column into one
  vim.g.signcolumn = 'number'
else
  vim.g.signcolumn = 'yes'
end

-- Set font (for GUIs)
vim.g.guifont = 'Hasklug NF:w08'

-- Neovide Stuff
vim.g.guifont = 'Hasklug NF'
if(vim.fn.exists('g:neovide') == 1) then
  print('Neovide in Effect!')
  vim.g.neovide_cursor_vfx_mode = "sonicboom"
  vim.g.neovide_refresh_rate = 140
end
