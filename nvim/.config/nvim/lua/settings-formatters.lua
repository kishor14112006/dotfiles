local formatters = {
  ecmascript = {
    -- prettier
    function()
      return {
        exe = "prettier",
        args = {"--stdin-filepath", vim.fn.fnameescape(vim.api.nvim_buf_get_name(0)), "--single-quote"},
        stdin = true
      }
    end
  },
  lua = {
    -- luafmt
    function()
      return {
        exe = "luafmt",
        args = {"--indent-count", 2, "--stdin"},
        stdin = true
      }
    end
  }
}
require("formatter").setup(
  {
    filetype = {
      javascript = formatters.ecmascript,
      javascriptreact = formatters.ecmascript,
      typescript = formatters.ecmascript,
      typescriptreact = formatters.ecmascript,
      lua = formatters.lua
    }
  }
)

vim.api.nvim_exec(
  [[
augroup FormatAutogroup
  autocmd!
  autocmd BufWritePost *.ts,*.tsx,*.js,*.rs,*.lua FormatWrite
augroup END
]],
  true
)
