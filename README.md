# Dotfiles
Wherever I go, there shall my dotfiles be also.

# Installing

Make sure Ansible is installed.  Then run:
```sh
sudo ansible-playbook ./ansible/main.yml
```

## Useful Variables

You can provide these variables to `ansible-playbook --extra-vars=""`
* `home`: Current user home directory
* `user`: Current user name
* `platform`: A string with value `linux64` or `macos`

## Useful Tags

Run specific portions of the Ansible playbook with these tags.  (Make sure to use the `--skip-tags "first-run` argument as well).
* `neovim`: Just install/update Neovim
* `support`: Just install supporting software (`nodejs`, `npm`, etc.)
* `lang-servers`: Just install language servers (requires `npm`)
* `dotfiles`: Just install dotfiles.

