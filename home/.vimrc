" Syntax highlighting + formatting
syntax on
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set smarttab
filetype plugin on
set nocompatible

" Line wrapping
set wrap
set linebreak
set showbreak=---->\  " Indicator for soft-wrap line breaks
set textwidth=0
set wrapmargin=0

" Folding
set foldmethod=indent
set foldnestmax=10
" Automatically collapse folds when file is opened.
set foldlevelstart=2

" Show whitespace
set list

" Show line at column 80
set colorcolumn=80,100
highlight ColorColumn ctermbg=darkgrey guibg=Grey3

" Highlight current line
set cursorline

" Show line numbers
set number

" Change file saving strategy
" Fixes issues w/ webpack hotreloading
set backupcopy=yes

" Sync Vim clipboard to system clipboard
if has('unix')
  " The `+` register is used by X-server
  set clipboard=unnamedplus
else
  set clipboard=unnamed
endif

" Mouse support
if has('mouse')
  set mouse=a
endif

" Show title in terminal
set title
set titleold="Terminal"
set titlestring=%F

" Allow :find to fuzzy-find files in `pwd`
set path=.,**

" Leave `x` lines when scrolling
set scrolloff=5

" -----------------------------------------------------------------------------
" Text Editing
" -----------------------------------------------------------------------------

" Move lines up/down
nnoremap <DOWN> :m .+1<CR>==
vnoremap <DOWN> :m '>+1<CR>gv=gv
nnoremap <UP> :m .-2<CR>==
vnoremap <UP> :m '<-2<CR>gv=gv

" Move word-wise with ALT key.
imap <A-LEFT> <ESC>bi
imap <A-RIGHT> <ESC>eli
imap <A-BACKSPACE> <ESC>diwa

" (In|De)dent lines with <LEFT>/<RIGHT>
nnoremap <S-TAB> <<
nnoremap <TAB> >>
vnoremap <S-TAB> <gv
vnoremap <TAB> >gv

" -----------------------------------------------------------------------------
" Search <space>s
" -----------------------------------------------------------------------------

" Engage case-insensitive search!
set ignorecase

" Make sure search highlight is prominant.
highlight Search ctermbg=yellow ctermfg=black guibg=yellow guifg=black

" Center cursor after search
nnoremap n nzz
nnoremap N Nzz

" Search for selected text
vnoremap <space>sf "hy/<c-r>h<CR>
" Select word if no text is selected
nmap <space>sf viw<space>sf

" Search and replace selected text
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

" Clear highlighting on escape in normal mode
nnoremap <silent> <esc> :noh<return><esc>
nnoremap <silent> <esc>^[ <esc>^[

" -----------------------------------------------------------------------------
" Buffer
" -----------------------------------------------------------------------------

" Alternate: Next / Previous Buffer
nmap <LEFT> :bp<CR>
nmap <RIGHT> :bn<CR>

" Close current buffer without losing split
" (Why this has to be an 'add-on' is beyond me... 😠)
nmap <silent> <space>bd :bp\|bd #<CR>

" Re-open recently closed buffer
nmap <space>bo <c-o><CR>

" Don't show QuickFix buffer when cycling buffers
augroup qf
    autocmd!
    autocmd FileType qf set nobuflisted
augroup END

" -----------------------------------------------------------------------------
" Jumps
" -----------------------------------------------------------------------------
nnoremap <c-j> <c-i>
nnoremap <c-k> <c-o>

" -----------------------------------------------------------------------------
" Windows / Splits <space>w
" -----------------------------------------------------------------------------

" Close windows
nmap <space>wd :q<CR>

" Open splits
nmap <space>ws :sp<CR>
nmap <space>wv :vs<CR>

" Resize splits
nmap <space>w= <c-w>=
" nmap <space>wrl :vertical res +10<CR>
" _map <space>wrh :vertical res -10<CR>
" nmap <space>wrk :res +10<CR>
" nmap <space>wrj :res -10<CR>
nmap          <space>wr+     <C-W>+<SID>ws
nmap          <space>wr-     <C-W>-<SID>ws
nn <script>   <SID>ws+   <C-W>+<SID>ws
nn <script>   <SID>ws-   <C-W>-<SID>ws
nmap          <SID>ws    <Nop>

" Move between splits
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" Move between splits (alternative method)
nnoremap <space>wh <c-w>h
nnoremap <space>wl <c-w>l
nnoremap <space>wj <c-w>j
nnoremap <space>wk <c-w>k

" Quit vim
nmap <space>qq :qa<CR>

" -----------------------------------------------------------------------------
" Terminals
" -----------------------------------------------------------------------------

" Terminal Emulator
function OpenTerminal(side) abort
  " Open a new split if applicable.
  if a:side == 'bottom'
    :sp
    :wincmd j
  elseif a:side == 'right'
    :vs
    :wincmd l
  endif

  :terminal
endfunction

function PrepTermBuffer() abort
  " Don't list the buffer.
  set nobuflisted

  " Set some keymaps for standard shells (but not stuff like FZF, etc.)
  if match(bufname('%'), 'zsh') >= 0
    " Allow ESC to exit "insert" mode in terminal buffer
    tnoremap <buffer> <Esc> <C-\><C-n>

    " Disable quick nav between buffers.
    nmap <buffer> <LEFT> :echo "Changing buffers in terminals is disabled."<CR>
    nmap <buffer> <RIGHT> :echo "Changing buffers in terminals is disabled."<CR>
  endif
endfunction

augroup termIgnore
  autocmd!
  if has('nvim')
    autocmd TermOpen * call PrepTermBuffer()
  else
    autocmd TerminalOpen * call PrepTermBuffer()
  end
augroup END

" Open terminal windows.
nmap <space>tt :call OpenTerminal()<CR>
nmap <space>tj :call OpenTerminal('bottom')<CR>
nmap <space>tl :call OpenTerminal('right')<CR>

" Move between splits (alternative method)
tnoremap <space>wh <c-w>h
tnoremap <space>wl <c-w>l
tnoremap <space>wj <c-w>j
tnoremap <space>wk <c-w>k

" -----------------------------------------------------------------------------
" Spelling <leader>s
" -----------------------------------------------------------------------------

" Toggle on/off
nmap <leader>st :set spell!<CR>
" Suggest replacment
nmap <leader>ss z=

" -----------------------------------------------------------------------------
" File Exploration <space>e
" -----------------------------------------------------------------------------

" Open netrw at file directory (i.e. "here")
nmap <space>eh :Vex<CR>
" Open netrw at project directory
nmap <space>ep :exe 'Vex' getcwd()<CR>

let g:netrw_banner = 0 " Hide the help banner
let g:netrw_liststyle = 3 " Long listing (file file per line w/ timestamp & size)
let g:netrw_browse_split = 4 " Open files in preview window
let g:netrw_winsize = 20 " Initial width of the window
let g:netrw_hide = 0 " Show hidden files
let g:netrw_keepdir = 0 " Commands act on the current visible directory

" Custom keybinds for NetRW buffers.
function! SetNetRwKeybinds()
  nmap <buffer> za <CR>
endfunction
au! FileType netrw call SetNetRwKeybinds()

" -----------------------------------------------------------------------------
" File Type Associations
" -----------------------------------------------------------------------------

autocmd BufNewFile,BufRead *.svelte set syntax=html

" -----------------------------------------------------------------------------
" External Tools
" -----------------------------------------------------------------------------

" Attempt to use Python to pretty-print JSON
nnoremap <leader>j :%!python -m json.tool<CR>
