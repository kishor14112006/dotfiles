# Pull in common settings.
source "$HOME/.shellrc"

# Set up FZF
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
